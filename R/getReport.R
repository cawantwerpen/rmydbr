#' MyDBR client
#'
#' This class provides a client for downloading reports from a MyDBR
#' application. It uses the direct url access functionality (
#' see https://mydbr.com/doc/content/manage.directurl.html). It is intented
#' to handle reports that return a single csv instance.
#'
#' @section Usage:
#' \preformatted{mydbr_client <- MydbrClient$new(base_url = 'https://example.org/report.php', user='exampleUser', password='secret', url_hash_seed='anotherSecret')
#'
#' mydbr_client$get_report(r=23, p1='My param', p2=5000, u1='2018-01-01')
#' }
#' @section Arguments:
#' \describe{
#'   \item{base_url}{Character scalar, the base url for the report-API of the MyDBR-instance}
#'   \item{user}{Character scalar, the username of a user with access to the MyDBR application and reports.}
#'   \item{password}{Character vector, the password for the speciefied user.}
#'   \item{url_hash_seed}{A character scalar, the seed used for generating parameter hashes. This is specific for
#'   each mydbr instance.}
#'}
#'
#' @section Details:
#' \code{$new()} instantiates a new MydbrClient.
#'
#' \code{$get_report()} returns the report as a dataframe. It takes any valid MyDBR
#' url query parameters (r: report_id, p1, p2, u1, u2, ...)
#'
#' @examples
#' mydbr_client <- MydbrClient$new(base_url = 'https://example.org/report.php', user='exampleUser', password='secret', url_hash_seed='anotherSecret')
#' mydbr_client$get_report(r=23, p1='My param', p2=5000, u1='2018-01-01')
#' @export
MydbrClient <- R6::R6Class("MydbrClient",
  public = list (
    baseurl = NA,

    initialize = function(baseurl, user, password, url_hash_seed) {
      private$user <- user
      private$password <- password
      private$url_hash_seed <- url_hash_seed
      self$baseurl <- baseurl
    },

    get_report = function (...) {
      params <- list(...)
      p <- sort(names(params[grep("p", names(params))]))
      h <- as.character(params[['r']])
      for (param in p) {
        h <- URLencode(as.character(paste(h, paste(param, params[param], sep=''), sep='')))
      }
      h <- as.character(paste(h, private$url_hash_seed, sep=''))
      params['h'] <- digest::digest(object=h, algo = 'sha1', serialize=FALSE)
      params['export'] <- 'csv'
      r <- httr::GET(self$baseurl,
                     query=params,
                     handle = httr::handle(self$baseurl),
                     config = httr::config(http_version=2),
                     httr::add_headers(`X-MYDBR-AUTH` = '1'),
                     httr::authenticate(user=private$user, password=private$password, type='basic')
           )
      return(httr::content(r))
    }
  ),

  private = list(
     user = NA,
     password = NA,
     url_hash_seed = NA
  )
)
